;; DONT SHOW STARTUP SCREEN
(setq inhibit-startup-message t)

;; CLEAN UP THE USER INTERFACE A BIT
(scroll-bar-mode -1)    ;; DISABLES THE VERTICAL SCROLLBAR
(tool-bar-mode -1)      ;; DISABLES THE ANNOYING OUTDATED TOOLBAR
(tooltip-mode -1)       ;; DISABLES ANNOYING TOOLTIPS
(set-fringe-mode 10)    ;; ALLOCATES THE SCROLLBARS ROOM
(menu-bar-mode 0)       ;; DISABLES THE ANNOYING MENU BAR

;; USE ESC TO QUIT PROMPTS
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

;; COLUMN NUMBERS
(column-number-mode)
(global-display-line-numbers-mode t)

;; DISABLE COLUMN NUMBERS FOR CERTIN MODES
(dolist (mode '(org-mode-hook
		term-mode-hook
		eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

;; ENHANCE THE UI A BIT
(setq visual-bell t)    ;; ENABLES VISUAL FEEDBACK WHEN YOU DO SOMETHING

;; STOP WARNINGS FROM COVERING THE WHOLE SCREEN
(setq warning-minimum-level :emergency)

;; ADD SOME STYLE TO THE UI
(set-face-attribute 'default nil :font "Fira Code Retina" :height 165) ;; THE FIRA CODE FONT
(load-theme 'doom-palenight) ;; LOAD A COLOR THEME FOR THE GUI VERSION EMACS

;; INSTALL PACKAGE
(require 'package)

;; INITIALIZE PACKAGE REPOSITORYS
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
			 ("org" . "https://orgmode.org/elpa/")
			 ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

;; INITALIZE USE-PACKAGE ON NON-LINUX PLATFORMS
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

;; INSTALL USE-PACKAGE
(require 'use-package)

;; ALWAYS ENSURE USE-PACKAGE
(setq use-package-always-ensure t)

;; DEBUGGING
   ;; COMMAND LOG MODE
   (use-package command-log-mode)

;; AUTO COMPLETION
   ;; IVY
   (use-package ivy
     :bind (("C-s" . swiper)
	    :map ivy-minibuffer-map
	    ("TAB" . ivy-alt-done)
	    ("C-l" . ivy-alt-done)
	    ("C-j" . ivy-next-line)
	    ("C-k" . ivy-previous-line)
	    :map ivy-switch-buffer-map
	    ("C-k" . ivy-previous-line)
	    ("C-l" . ivy-done)
	    ("C-d" . ivy-switch-buffer-kill)
	    :map ivy-reverse-i-search-map
	    ("C-k" . ivy-previous-line)
	    ("C-d" . ivy-reverse-i-search-kill))
     :config
     (ivy-mode 1))

     ;; IVY RICH
     (use-package ivy-rich
       :init
       (ivy-rich-mode 1))

     ;; COUNSEL
     (use-package counsel
       :bind (("M-x" . counsel-M-x)
	      ("C-x b" . counsel-ibuffer)
	      ("C-x C-f" . counsel-find-file)
	      :map minibuffer-local-map
	      ("C-r" . 'counsel-minibuffer-history))
       :config
       (setq ivy-initial-inputs-alist nil))
     
;; APPEARANCE
   ;; DOOM MODELINE
   (use-package doom-modeline
     :ensure t
     :init (doom-modeline-mode 1)
     :custom ((doom-modeline-height 50)))

   ;; DOOM THEMES
   (use-package doom-themes)

   ;; RAINBOW DELIMITERS
   (use-package rainbow-delimiters
     :hook (prog-mode . rainbow-delimiters-mode))

;; INFO
   ;; HELPFUL
   (use-package helpful
     :ensure t
     :custom
     (counsel-describe-function-function #'helpful-callable)
     (counsel-describe-variable-function #'helpful-variable)
     :bind
     ([remap describe-function] . counsel-describe-function)
     ([remap describe-command] . helpful-command)
     ([remap describe-variable] . counsel-describe-variable)
     ([remap describe-key] . helpful-key))

;; INPUT
   ;; WHICH KEY
   (use-package which-key
     :init (which-key-mode)
     :diminish which-key-mode
     :config
     (setq which-key-idle-delay 1))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("c83c095dd01cde64b631fb0fe5980587deec3834dc55144a6e78ff91ebc80b19" default))
 '(doom-modeline-mode t)
 '(package-selected-packages
   '(helpful counsel ivy-rich which-key use-package rainbow-delimiters ivy doom-themes doom-modeline command-log-mode))
 '(which-key-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
