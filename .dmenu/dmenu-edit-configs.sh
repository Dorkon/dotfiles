#!/bin/bash

declare options=("xmonad
qtile
alacritty
bash
emacs.d/init.el
neovim
quit")

choice=$(echo -e "${options[@]}" | dmenu -i -p 'Edit config file: ')

case "$choice" in
	quit)
		echo "Program terminated." && exit 1
	;;

        xmonad)
		choice="$HOME/.xmonad/xmonad.hs"
        ;;

	alacritty)
		choice="$HOME/.config/alacritty/alacritty.yml"
	;;
	bash)
		choice="$HOME/.bashrc"
	;;
	emacs.d/init.el)
		choice="$HOME/.emacs.d/init.el"
	;;
	neovim)
		choice="$HOME/.config/nvim/init.vim"
	;;
	qtile)
		choice="$HOME/.config/qtile/config.py"
	;;
	*)
		exit 1
	;;
esac
emacsclient -c -a emacs "$choice"
